<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"> 
<title>exercice2</title> 
</head>

<body>
<h1 style="font-size:130%;">Réponse N°1</h1> <br>
<table style="width:30%";  border="1px";>
  <tr>
    <th colspan=2>Etudiants</th>
  </tr>
  <tr>
    <td align = "center";>Numéro</td>
    <td align = "center";>Prénom</td>
  </tr>
  <tr>
    <td align = "center";> 
    	<?php $numero = array(1, 2, 3, 4, 5); 
        for($x = 0; $x < sizeof($numero); $x++) {
            echo $numero[$x];
            echo '<br>';
        }
        ?>
    </td>
    <td align = "center";> 
    	<?php $prenom = array("Ali", "Kamel", "Salwa", "Abir", "Mourad");
    	for($x = 0; $x < sizeof($prenom); $x++) {
    	    echo $prenom[$x];
    	    echo '<br>';
    	}
    	?> 
    </td>
  </tr>
</table> <br>
<h1 style="font-size:130%;">Réponse N°2</h1> <br>
<table style="width:30%";  border="1px";>
  <tr>
    <th colspan=2>Notes</th>
  </tr>
  <tr>
    <td align = "center";>Numéro</td>
    <td align = "center";>Note</td>
  </tr>
  <tr>
    <td align = "center";> 
    	<?php $numero = array(1, 2, 3, 4, 5); 
        for($x = 0; $x < sizeof($numero); $x++) {
            echo $numero[$x];
            echo '<br>';
        }
        ?>
    </td>
    <td align = "center";> 
    	<?php $notes = array(14, 19, 9, 4, 11);
    	for($x = 0; $x < sizeof($notes); $x++) {
    	    echo $notes[$x];
    	    echo '<br>';
    	}
    	?> 
    </td>
  </tr>
</table> <br>
<h1 style="font-size:130%;">Réponse N°3</h1>
<p>
	<?php 
	$max = max($notes);
	$position = array_search($max, $notes);
	echo "L'étudiant qui a eu la meilleure note est ".$prenom[$position];
	?> 
</p>
<h1 style="font-size:130%;">Réponse N°4</h1>
<p>
	<?php 
	$min = min($notes);
	$position = array_search($min, $notes);
	echo "L'étudiant qui a eu la mauvaise note est ".$prenom[$position];
	?> 
</p>
<h1 style="font-size:130%;">Réponse N°5</h1>
<p>
	<?php
    $somme_notes = 0;
    $i = 0;
    foreach($notes as $cle=>$valeur)
    {
    $i++; 
    $somme_notes+=$valeur;
    }
    $moyenne = $somme_notes / $i;
    echo "La moyenne des notes est " .$moyenne;
    ?>
</p>
<h1 style="font-size:130%;">Réponse N°6</h1>
<p>
	<?php
	if ($moyenne>=15){
	    echo "Oui";
	}
	else {
	    echo "Non";
	}
    ?>
</p>
<h1 style="font-size:130%;">Réponse N°7</h1> <br>
<table style="width:30%";  border="1px";>
  <tr>
    <th colspan=2>Etudiants-Notes</th>
  </tr>
  <tr>
    <td align = "center";>Prénom</td>
    <td align = "center";>Note</td>
  </tr>
  <tr>
    <td align = "center";> 
    	<?php $prenom = array("Ali", "Kamel", "Salwa", "Abir", "Mourad");
    	for($x = 0; $x < sizeof($prenom); $x++) {
    	    echo $prenom[$x];
    	    echo '<br>';
    	}
    	?> 
    </td>
    <td align = "center";> 
    	<?php $notes = array(14, 19, 9, 4, 11);
    	for($x = 0; $x < sizeof($notes); $x++) {
    	    echo $notes[$x];
    	    echo '<br>';
    	}
    	?> 
    </td>
  </tr>
</table> <br>
</body>
</html> 
