<?php
if (isset($_POST['boucle_for']))
{
    BoucleFor($x=1);
}
else if (isset($_POST['boucle_while']))
{
    BoucleWhile($x=1);
}
function BoucleWhile ($x){
    echo "Les chiffres sont avec la boucle while:";
    echo '<br>';
    echo '<br>';
    $x=1;
    while (($x <= 50) && ($x >= 1))  {
        echo "$x*";
        $x++;
    }
}
function BoucleFor ($x){
    echo '<br>';
    echo "Les chiffres sont avec la boucle for:";
    echo '<br>';
    for ($x = 1; $x <= 50; $x++){
        echo "$x*";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"> 
<title>exercice2</title> 
</head>
<body>

<form action="exercice2.php" method="post">
<input type="submit" name="boucle_for" value="boucle-for"><br>
<input type="submit" name="boucle_while" value="boucle-while"><br>
</form>

</body>
</html> 